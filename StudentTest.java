public class StudentTest {
	public static void main(String[] args) {
		Student[] data = {
			new Student("マイク", 65, 90, 100),
			new Student("ポップ", 82, 73, 64),
			new Student("コーン", 74, 31, 42),
			new Student("食べたい", 100, 95, 99),
		};
		for (int i = 0; i < data.length; i++) {
			System.out.println(data[i] + "\t->" + data[i].total());
		}
 	}
}